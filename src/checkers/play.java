package checkers;

import java.util.*;

public class play {
    public static void main(String[] args){
       int[][] init_board = new int[8][8];
//               new int[][]{{0, -1, 0, -1, 0, -1, 0, -1},
//                                        {-1, 0, 0, 0, -1, 0, -1, 0},
//                                        {0, -1, 0, -1, 0, 0, 0, 0},
//                                        {0, 0, 0, 0, -1, 0, 0, 0},
//                                        {0, 0, 0, 1, 0, -1, 0, 1},
//                                        {1, 0, 1, 0, 0, 0, 1, 0},
//                                        {0, 0, 0, 1, 0, 0, 0, 1},
//                                        {1, 0, 1, 0, 1, 0, 1, 0}};
//        init_board[0][7] = 2;
//        init_board[1][6] = 1;
//        init_board[5][6] = -1;
//        init_board[6][5] = -1;


        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 8; j++){
                if((i + j) % 2 != 0)
                    init_board[i][j] = -1;
            }
        }
        for(int i = 5; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if((i + j) % 2 != 0)
                    init_board[i][j] = 1;
            }
        }

        gameTree g = new gameTree(1, init_board);
        g.printBoard();

        Scanner in = new Scanner(System.in);
        while(!g.isEnd()) {
            if (g.player > 0) {
                int[] mv = new int[4];
                List<int[]> havefood = g.haveFood();
                if (havefood.size() > 0) {
                    System.out.println("Player" + (g.player * (-1) + 3) / 2 + " Please select a move");
                    for (int i = 0; i < havefood.size(); i++) {
                        int[] f = havefood.get(i);
                        System.out.println(i + ". (" + f[0] + "," + f[1] + ") -> (" + f[4] + "," + f[5] + ")");
                    }
                    int[] selection = havefood.get(in.nextInt());
                    g = g.eat(selection);
                    g.printBoard();
                    while (g.hasFood(selection[4], selection[5]).size() > 0) {
                        System.out.println("Player" + (g.player * (-1) + 3) / 2 + " Please select a move");
                        List<int[]> hasfood = g.hasFood(selection[4], selection[5]);
                        for (int i = 0; i < hasfood.size(); i++) {
                            int[] f = hasfood.get(i);
                            System.out.println(i + ". (" + f[0] + "," + f[1] + ") -> (" + f[4] + "," + f[5] + ")");
                        }
                        selection = hasfood.get(in.nextInt());
                        g = g.eat(selection);
                        g.printBoard();
                    }
                    g.player = g.player * -1;
                } else {
                    System.out.println("Player" + (g.player * (-1) + 3) / 2 + " Please make a move:");
                    do {
                        String s = in.next();
                        if (s.equals("exit")) return;
                        String[] split = s.split(",");
                        for (int i = 0; i < 4; i++)
                            mv[i] = Integer.parseInt(split[i]);
                        if (!validMove(g, mv)) {
                            System.out.println("Invalid move, please make a selection again:");
                        }
                    } while (!validMove(g, mv));

                    g = g.move(mv[0], mv[1], mv[2], mv[3]);
                    g.printBoard();
                    g.player = g.player * -1;

                }
//        for(int i = 0; i < 1000; i++){
//            g.autoPlay();
//        }
//        System.out.println("win = " + g.win + " " + "total = " + g.total);
            } else {
                for(int i = 0; i < 50; i++) {
                    gameTree child = g.generatechild();
                    child.setId();
                    if (g.children.containsKey(child.gameTreeId))
                        child = g.children.get(child.gameTreeId);
                    for (int j = 0; j < 50; j++) {
                        child.autoPlay();
                    }
                    g.children.put(child.gameTreeId, child);
                }
                double min = 1.1;
                String minId = "";
                for(String k : g.children.keySet()){
                    gameTree child = g.children.get(k);
                    double rate = (double)child.win / child.total;
                    if(rate < min){
                        min = rate;
                        minId = child.gameTreeId;
                    }
                }
                g = g.children.get(minId);
                g.printBoard();
            }
        }

        if(g.winner > 0)
            System.out.println("You win!");
        else {
            g.printBoard();
            System.out.println("Agent win!");
        }
        return;
    }

    public static boolean validMove(gameTree g, int[] mv){
        if (Math.abs(mv[2] - mv[0]) > 2 || Math.abs(mv[2] - mv[0]) < 1) return false;
        else if (Math.abs(mv[3] - mv[1]) > 2 || Math.abs(mv[3] - mv[1]) < 1) return false;
        else if (Math.abs(mv[3] - mv[1]) != Math.abs(mv[3] - mv[1])) return false;
        else if (mv[0] < 0 || mv[0] > 7 || mv[1] < 0 || mv[1] > 7 || mv[2] < 0 || mv[2] > 7 || mv[3] < 0 || mv[3] > 7) return false;
        else if (g.board[mv[0]][mv[1]] * g.player <= 0) return false;
        else if (g.board[mv[2]][mv[3]] != 0) return false;
        else if (Math.abs(mv[1] - mv[0]) == 2 && g.board[(mv[0] + mv[2]) / 2][(mv[1] + mv[3]) / 2] * g.board[mv[0]][mv[1]] >= 0) return false;
        else return true;
    }
}
