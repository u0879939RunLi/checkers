package checkers;

import java.util.*;
import java.io.*;

public class gameTree {

    public int player;
    public int winner;
    public int[][] board;
    public int win;
    public int total;
    public Map<String, gameTree> children;
    public String gameTreeId;

    public gameTree(int player, int[][] board){
        this.player = player;
        this.board = new int[8][8];
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                this.board[i][j] = board[i][j];
            }
        }
        this.winner = 0;
        this.children = new HashMap();
        this.win = 0;
        this.total = 0;
    }

    public void setId(){
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                sb.append(this.board[i][j]);
            }
        }
        this.gameTreeId = sb.toString();
    }

    public gameTree move(int a, int b, int newa, int newb){
        int[][] newBoard = new int[8][8];
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                newBoard[i][j] = board[i][j];
            }
        }
        newBoard[newa][newb] = newBoard[a][b];
        newBoard[a][b] = 0;
        if(newBoard[newa][newb] == -1 && newa == 7) newBoard[newa][newb] = -2;
        if(newBoard[newa][newb] == 1 && newa == 0) newBoard[newa][newb] = 2;
        gameTree child = new gameTree(player, newBoard);
//        if(!this.children.containsKey(child.gameTreeId))
//            children.put(child.gameTreeId, child);
        return child;
    }

    public gameTree eat(int[] food){
        int[][] newBoard = new int[8][8];
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                newBoard[i][j] = board[i][j];
            }
        }
        newBoard[food[0]][food[1]] = 0;
        newBoard[food[2]][food[3]] = 0;
        newBoard[food[4]][food[5]] = board[food[0]][food[1]];
        if(newBoard[food[4]][food[5]] == -1 && food[4] == 7) newBoard[food[4]][food[5]] = -2;
        if(newBoard[food[4]][food[5]] == 1 && food[4] == 0) newBoard[food[4]][food[5]] = 2;
        gameTree child = new gameTree(player, newBoard);
        if(!this.children.containsKey(child.gameTreeId))
            children.put(child.gameTreeId, child);
        return child;

    }

    public List<int[]> hasFood(int i, int j){
        List<int[]> res = new ArrayList();
        if(board[i][j] == 1){
            if(i > 1 && j > 1 && board[i-1][j-1] < 0 && board[i-2][j-2] == 0)
                res.add(new int[]{i, j, i-1, j-1, i-2, j-2});
            if(i > 1 && j < 6 && board[i-1][j+1] < 0 && board[i-2][j+2] == 0)
                res.add(new int[]{i, j, i-1, j+1, i-2, j+2});
        }
        if(board[i][j] == 2){
            if(i > 1 && j > 1 && board[i-1][j-1] < 0 && board[i-2][j-2] == 0)
                res.add(new int[]{i, j, i-1, j-1, i-2, j-2});
            if(i > 1 && j < 6 && board[i-1][j+1] < 0 && board[i-2][j+2] == 0)
                res.add(new int[]{i, j, i-1, j+1, i-2, j+2});
            if(i < 6 && j > 1 && board[i+1][j-1] < 0 && board[i+2][j-2] == 0)
                res.add(new int[]{i, j, i+1, j-1, i+2, j-2});
            if(i < 6 && j < 6 && board[i+1][j+1] < 0 && board[i+2][j+2] == 0)
                res.add(new int[]{i, j, i+1, j+1, i+2, j+2});
        }
        if(board[i][j] == -1){
            if(i < 6 && j < 6 && board[i+1][j+1] > 0 && board[i+2][j+2] == 0)
                res.add(new int[]{i, j, i+1, j+1, i+2, j+2});
            if(i < 6 && j > 1 && board[i+1][j-1] > 0 && board[i+2][j-2] == 0)
                res.add(new int[]{i, j, i+1, j-1, i+2, j-2});
        }
        if(board[i][j] == -2){
            if(i > 1 && j > 1 && board[i-1][j-1] > 0 && board[i-2][j-2] == 0)
                res.add(new int[]{i, j, i-1, j-1, i-2, j-2});
            if(i > 1 && j < 6 && board[i-1][j+1] > 0 && board[i-2][j+2] == 0)
                res.add(new int[]{i, j, i-1, j+1, i-2, j+2});
            if(i < 6 && j > 1 && board[i+1][j-1] > 0 && board[i+2][j-2] == 0)
                res.add(new int[]{i, j, i+1, j-1, i+2, j-2});
            if(i < 6 && j < 6 && board[i+1][j+1] > 0 && board[i+2][j+2] == 0)
                res.add(new int[]{i, j, i+1, j+1, i+2, j+2});
        }
        return res;
    }

    public List<int[]> haveFood(){
        List<int[]> res = new ArrayList();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(board[i][j] * player > 0) res.addAll(hasFood(i, j));
            }
        }
        return res;
    }

    public List<int[]> haveMove(){
        List<int[]> res = new ArrayList<>();
        if(player < 0){
            for(int i = 0; i < 8; i++){
                for(int j = 0 ; j < 8; j++){
                    if(this.board[i][j] == -1){
                        if(i < 7 && j > 0 && board[i+1][j-1] == 0)
                            res.add(new int[]{i, j, i+1, j-1});
                        if(i < 7 && j < 7 && board[i+1][j+1] == 0)
                            res.add(new int[]{i, j, i+1, j+1});
                    }
                    if(this.board[i][j] == -2){
                        if(i > 0 && j > 0 && board[i-1][j-1] == 0)
                            res.add(new int[]{i, j, i-1, j-1});
                        if(i < 7 && j > 0 && board[i+1][j-1] == 0)
                            res.add(new int[]{i, j, i+1, j-1});
                        if(i > 0 && j < 7 && board[i-1][j+1] == 0)
                            res.add(new int[]{i, j, i-1, j+1});
                        if(i < 7 && j < 7 && board[i+1][j+1] == 0)
                            res.add(new int[]{i, j, i+1, j+1});
                    }
                }
            }
        }
        else{
            for(int i = 0; i < 8; i++){
                for(int j = 0 ; j < 8; j++){
                    if(this.board[i][j] == 2){
                        if(i > 0 && j > 0 && board[i-1][j-1] == 0)
                            res.add(new int[]{i, j, i-1, j-1});
                        if(i < 7 && j > 0 && board[i+1][j-1] == 0)
                            res.add(new int[]{i, j, i+1, j-1});
                        if(i > 0 && j < 7 && board[i-1][j+1] == 0)
                            res.add(new int[]{i, j, i-1, j+1});
                        if(i < 7 && j < 7 && board[i+1][j+1] == 0)
                            res.add(new int[]{i, j, i+1, j+1});
                    }
                    if(this.board[i][j] == 1){
                        if(i > 0 && j > 0 && board[i-1][j-1] == 0)
                            res.add(new int[]{i, j, i-1, j-1});
                        if(i > 0 && j < 7 && board[i-1][j+1] == 0)
                            res.add(new int[]{i, j, i-1, j+1});
                    }
                }
            }
        }
        return res;
    }

    public boolean isEnd(){
        if(haveFood().size() == 0 && haveMove().size() == 0){
            winner = player * -1;
            return true;
        }
        else return false;
    }

    public void printBoard(){
        System.out.print("  ");
        for (int j = 0; j < 8; j++) {
            System.out.print("  " + j + " ");
        }
        System.out.println("");
        for(int i = 0; i < 16; i++){
            if(i % 2 == 0) {
                System.out.print("  ");
                for (int j = 0; j < 9; j++) {
                    if(j == 8) System.out.println("+");
                    else System.out.print("+---");
                }
            }
            else{
                System.out.print((i-1)/2 + " ");
                for (int j = 0; j < 9; j++) {
                    if(j == 8) System.out.println("|");
                    else if (board[(i-1)/2][j] == -2)
                        System.out.print("|(O)");
                    else if (board[(i-1)/2][j] == -1)
                        System.out.print("| o ");
                    else if (board[(i-1)/2][j] == 0)
                        System.out.print("|   ");
                    else if (board[(i-1)/2][j] == 1)
                        System.out.print("| x ");
                    else if (board[(i-1)/2][j] == 2)
                        System.out.print("|(X)");
                }
            }
        }
        System.out.print("  ");
        for (int j = 0; j < 9; j++) {
            if(j == 8) System.out.print("+");
            else System.out.print("+---");
        }
        System.out.println("");
    }
    public void autoPlay(){
        gameTree copy = new gameTree(player, board);
        Random rand = new Random();
        int cnt = 0;
        while(!copy.isEnd()){
//            copy.printBoard();
            List<int[]> food = copy.haveFood();
            List<int[]> move = copy.haveMove();
            if(food.size() > 0) {
                cnt = 0;
                int[] decision = food.get(rand.nextInt(food.size()));
                copy.board[decision[0]][decision[1]] = 0;
                copy.board[decision[2]][decision[3]] = 0;
                copy.eatAndSetKing(decision);

                while(copy.hasFood(decision[4], decision[5]).size() > 0){
                    List<int[]> nextfood = copy.hasFood(decision[4], decision[5]);
                    decision = nextfood.get(rand.nextInt(nextfood.size()));
                    copy.board[decision[0]][decision[1]] = 0;
                    copy.board[decision[2]][decision[3]] = 0;
                    copy.eatAndSetKing(decision);
                }
            }
            else if(move.size() > 0 && cnt < 50){
                int[] decision = move.get(rand.nextInt(move.size()));
                copy.board[decision[0]][decision[1]] = 0;
                copy.moveAndSetKing(decision);
                cnt++;
            }
            if(cnt >= 50) {
                this.total++;
                return;
            }
            copy.player *= -1;
        }
        if(copy.winner == this.player)
            this.win++;
        this.total++;
    }

    public gameTree generatechild() {
        gameTree copy = new gameTree(player, board);
        Random rand = new Random();
        List<int[]> food = copy.haveFood();
        List<int[]> move = copy.haveMove();
        if(food.size() > 0) {
            int[] decision = food.get(rand.nextInt(food.size()));
            copy.eatAndSetKing(decision);
            copy.board[decision[0]][decision[1]] = 0;
            copy.board[decision[2]][decision[3]] = 0;

            while(copy.hasFood(decision[4], decision[5]).size() > 0){
                List<int[]> nextfood = copy.hasFood(decision[4], decision[5]);
                decision = nextfood.get(rand.nextInt(nextfood.size()));
                copy.eatAndSetKing(decision);
                copy.board[decision[0]][decision[1]] = 0;
                copy.board[decision[2]][decision[3]] = 0;

            }
        }
        else if(move.size() > 0){
            int[] decision = move.get(rand.nextInt(move.size()));
            copy.board[decision[0]][decision[1]] = 0;
            copy.moveAndSetKing(decision);
        }
        copy.player *= -1;
        return copy;

    }
    public void eatAndSetKing(int[] a){
        if (player > 0) {
            if (a[4] == 0)
                this.board[a[4]][a[5]] = 2;
            else if(this.board[a[0]][a[1]] == 2)
                this.board[a[4]][a[5]] = 2;
            else
                this.board[a[4]][a[5]] = 1;
        } else {
            if(a[4] == 7)
                this.board[a[4]][a[5]] = -2;
            else if(this.board[a[0]][a[1]] == -2)
                this.board[a[4]][a[5]] = -2;
            else
                this.board[a[4]][a[5]] = -1;
        }
    }
    public void moveAndSetKing(int[] a){
        if (player > 0) {
            if (a[2] == 0)
                this.board[a[2]][a[3]] = 2;
            else if(this.board[a[0]][a[1]] == 2)
                this.board[a[2]][a[3]] = 2;
            else
                this.board[a[2]][a[3]] = 1;
        } else {
            if(a[2] == 7)
                this.board[a[2]][a[3]] = -2;
            else if(this.board[a[0]][a[1]] == -2)
                this.board[a[2]][a[3]] = -2;
            else
                this.board[a[2]][a[3]] = -1;
        }
    }
}
